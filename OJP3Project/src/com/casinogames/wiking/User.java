package com.casinogames.wiking;

public interface User {
	public void getPoints(double points);
	public void lostPoints(double points);
	public void printBalance();
	public String getName();
	public double getBalance();
	
}
