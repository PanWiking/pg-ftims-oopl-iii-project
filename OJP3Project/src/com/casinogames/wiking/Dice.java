package com.casinogames.wiking;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
// Simulate game dice on 'homManyWalls' walls
public class Dice  {
	private int howManyWalls;
	
	public Dice(int howManyWalls2) {
		this.howManyWalls=howManyWalls2;
	}
	public int getHowManyWalls() {
		return howManyWalls;
	}

	public void setHowManyWalls(int howManyWalls) {
		this.howManyWalls = howManyWalls;
	}

	public int getRandom(){
		//True random;
		Random rand =new Random ();
		Map<Integer,Integer> out=new HashMap<Integer,Integer>();
		for (int i = 0; i < 100*howManyWalls; i++) {
			int r=rand.nextInt(howManyWalls)+1;
			Integer frequency=out.get(r);
			out.put(r, frequency==null?1:frequency+1);
		}
		
		int q=0;
		for (int i = 1; i < out.size()+1; i++) {
			if(out.get(i).intValue()>q){
				q=out.get(i).intValue();
			}
		}
		return q%howManyWalls+1;
		
	}

}
