package com.casinogames.wiking;

public class Pair {
	private int a;
	private int b;
	private int lp;
	private boolean win=false;
	public boolean isWin() {
		return win;
	}
	public void setWin(boolean win) {
		this.win = win;
	}
	public int getA() {
		return a;
	}
	public void setA(int a) {
		this.a = a;
	}
	public int getB() {
		return b;
	}
	public void setB(int b) {
		this.b = b;
	}
	Pair(int lp,int a,int b){
		this.lp=lp;
		this.a=a;
		this.b=b;
	}
}
