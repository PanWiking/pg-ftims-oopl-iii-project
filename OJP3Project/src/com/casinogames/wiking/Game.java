package com.casinogames.wiking;

import java.util.LinkedList;
import java.util.Scanner;

public interface Game {
	public boolean again(Scanner readingObj);
	
	public void startGame(LinkedList<User> a);

	void whoWin(LinkedList<User> a);
}
