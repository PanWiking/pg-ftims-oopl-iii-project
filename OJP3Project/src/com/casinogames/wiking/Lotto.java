package com.casinogames.wiking;

import java.util.LinkedList;
import java.util.Scanner;
//main game
public class Lotto implements Game{
	LinkedList<Pair> votes=new LinkedList<>();
	private void compere(){
		Dice dice=new Dice(20);
		Pair winNumbers=new Pair(0,dice.getRandom(),dice.getRandom());
		for(int i=0;i<votes.size();i++){
			if((votes.get(i).getA()==winNumbers.getA()&&
				votes.get(i).getB()==winNumbers.getB())||(
				votes.get(i).getA()==winNumbers.getB()&&
				votes.get(i).getB()==winNumbers.getA())){
				votes.get(i).setWin(true);
			}
		}
		System.out.println("Wining numbers is "+winNumbers.getA()+" "+winNumbers.getB());
	}
	@Override
	public void startGame(LinkedList<User> a) {
		@SuppressWarnings("resource")
		Scanner readingObj=new Scanner(System.in);
		System.out.println("Pleas chose ur 2 numbers from 1 to 20");
		
		for(int i=0;i<a.size();i++){
			a.get(i).lostPoints(10);
			System.out.println("Player "+a.get(i).getName()+" give the number of the first dice");
			int first=readingObj.nextInt();
			System.out.println("Player "+a.get(i).getName()+" give the number of the secnd dice");
			int second=readingObj.nextInt();
			Pair newPar=new Pair(i+1, first, second);
			votes.add(newPar);
		}
		whoWin(a); 
		System.out.println();
	}

	@Override
	public void whoWin(LinkedList<User> a) {
		compere();
		
		for(int i=0; i<votes.size();i++){
			if(votes.get(i).isWin()){
				System.out.println("Player "+a.get(i).getName()+" win");
				a.get(i).getPoints(a.size()*10);
			}
		}
	}
	@Override
	public boolean again(Scanner readingObj) {
		Comparable<String> decysion = null ;
		System.out.println("Do you want play again");
		decysion=readingObj.nextLine();
		if(decysion.equals("yes")){
			return true;
		}
		else{
			return false;
		}
	}



}
