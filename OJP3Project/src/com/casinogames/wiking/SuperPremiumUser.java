package com.casinogames.wiking;

public class SuperPremiumUser extends PremiumUser{
// class for haks 
	public SuperPremiumUser(int lp, String name, double balance) {
		super(lp, name, balance);
		this.name=name;
		this.balance=balance+1000;
		this.premium=true;
	}
	@Override
	public void lostPoints(double points) {
		balance+=points;
		
	}
	@Override
	public void setPremium(NormalUser a){
		this.name=a.name;
		this.balance=a.balance+1000;
		this.premium=true;
	}


}
