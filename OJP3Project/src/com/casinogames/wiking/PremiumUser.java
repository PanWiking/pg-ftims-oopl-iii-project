package com.casinogames.wiking;

public class PremiumUser extends NormalUser {
	
	public PremiumUser(int lp, String name,double balance) {
		super(lp,name, balance);
		this.name=name;
		this.balance=balance;
		this.premium=true;
		
	}

	@Override
	public void getPoints(double points) {
		balance+=points;
	}
	
	public void setPremium(NormalUser a){
		this.name=a.name;
		this.balance=a.balance-10;
		this.premium=true;
	}

	
	
}
