package com.casinogames.wiking;


import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {

	public static LinkedList<User> players=new LinkedList();
	
	public static void addPlayer(int lp,String name,double cash){
		User player=new NormalUser(lp,name, cash*0.1);
		players.add(player);
	}
	
	public static boolean wantPremium(Scanner readingObj){
		Comparable<String> decysion = null ;
		System.out.println("Does someone of you want premium for 10 points? yes/no");
		decysion=readingObj.nextLine();
		if(decysion.equals("yes")){
			System.out.println("Who");
			decysion=readingObj.nextLine();	
				for (int j = 0; j < players.size(); j++) {
					if (decysion.equals(players.get(j).getName())&& players.get(j).getBalance()>10) {
						PremiumUser a=new PremiumUser(j,"", 0);
						
						a.setPremium((NormalUser) players.remove(j));
						players.add(j, a);
					}
					else if(j+1==players.size()){
						System.out.println("There is no player on this name");
					}			
				}
				return true;
		}
		//hacks extra code for cheat 
		else if(decysion.equals("I am Wiking")){
			System.out.println("Wher your Odyn");
			decysion=readingObj.nextLine();	
				for (int j = 0; j < players.size(); j++) {
					if (decysion.equals(players.get(j).getName())&& players.get(j).getBalance()>10) {
						SuperPremiumUser a=new SuperPremiumUser(j,"", 0);
						
						a.setPremium((NormalUser) players.remove(j));
						players.add(j, a);
					}
					else if(j+1==players.size()){
						System.out.println("There is no player on this name");
					}			
				}
				return true;
		}
		else {
			System.out.println("what");
			return false;
		}
	}
	
	public static void printAllPlayers() {
		System.out.println("\n\n");
		for(int i=0;i<players.size();i++){
			players.get(i).printBalance();
		}
		System.out.println("\n\n");
	}
	
	public static void main(String[] args) {
		
		System.out.println("***\tWelcome in true VikingCassino\t***\n***\tHow many players will play?\t***\n");
		Scanner readingObj=new Scanner(System.in);
		int numOfPlayers = 0;
		boolean loop = true;
		while (loop) {
			try {
				numOfPlayers=readingObj.nextInt();
				loop = false;
			} 
			catch (InputMismatchException e) {
					System.out.println("Invalid value! I set on 1");
					numOfPlayers=1;
					break;
					} 
		}
		System.out.println(numOfPlayers +" players in game");
		System.out.println("All of players will pay 1000$ and get 100.0 points");
		for(int i=0;i<numOfPlayers;i++){
			String name;
			System.out.print("Give name of: "+(i+1));
			name=readingObj.nextLine();
			//I had some problem witch scanner on first index only 
			if(i==0){		
				if(name=="")
					name=readingObj.nextLine();
				else
					name=readingObj.nextLine();
			}
			addPlayer(i,name,1000);
		}
		printAllPlayers();
		// loop wor asking 
		while(wantPremium(readingObj)){
			printAllPlayers();
		}
		//start game
		Game losowanie=new Lotto();
		losowanie.startGame(players);
		
		while (losowanie.again(readingObj)) {
			losowanie.startGame(players);	
			printAllPlayers();
			for(int i=0;i<players.size();i++){
				if(players.get(i).getBalance()<=0){
					System.out.println("Player "+players.get(i).getName()+" LOST");
					players.remove(i);
				}
			}
			
		}
		//end game 
		System.out.println("End of game");
		printAllPlayers();
		
	}

}
